const express = require('express');
const repo = require("./repository/repo")

var fs = require('fs');
var http = require('http');
var https = require('https');


const app = express();
const path = require('path');
const router = express.Router();

router.get('/index', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/hello.html'));
    //__dirname : It will resolve to your project folder.
});

router.get('/quick', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/quick.html'));
    //__dirname : It will resolve to your project folder.
});

router.get('/quickDb', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/quickdb.html'));
    //__dirname : It will resolve to your project folder.
});

router.get('/message', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/messages.html'));
    //__dirname : It will resolve to your project folder.
});


//add the router
app.use('/', router);
// get all todos

app.get('/api/text', (req, res) => {
    if (req.query.text) {
        res.status(200).send({
            message: req.query.text.toUpperCase()
        })
    } else {
        res.status(401).send({
            Error: "please specify message"
        })

    }
});


app.get('/api/textDb', async (req, res) => {

    console.log("Calling database function");

    console.log(repo);
    await repo.InsertSimple(req.query.text);

    console.log("DB function called");
    res.status(200).send({
        message: req.query.text.toUpperCase()
    })



});

app.get('/api/NewMessage', async (req, res) => {

    console.log("Calling database function");

    //console.log(req.query);

    var post = await repo.InsertUserPost(req.query.username, req.query.post);

    console.log(post.ops);
    console.log("DB function called");
    res.status(200).send(
        post.ops);
});

app.get('/api/Reply', async (req, res) => {

    console.log("Calling database function");
    console.log(req.query);
    var post = await repo.InsertUserPostComment(req.query.id, req.query.reply);
    console.log(post);
    console.log("DB function called");
    res.status(200).send({
        // post.ops[0]);
    });
});

app.get('/api/test', async (req, res) => {

    console.log("Calling database function");

    console.log(repo);

    await repo.InsertUserPost("aman", "farooq");

    await repo.InsertUserPostComment("aman", "farooq", "student");


    console.log("DB function called");
    res.status(200).send()

    res.status(401).send({
        Error: "please specify message"
    })


});


app.get('/', (req, res) => {
    res.sendFile("~/index.html");
})

var privateKey = fs.readFileSync('server.key');
var certificate = fs.readFileSync('server.cert');

var credentials = { key: privateKey, cert: certificate };
console.log(credentials);

// your express configuration here

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(80);
httpsServer.listen(443, (r, e) => {
    console.log("listining");
    console.log(e);
});
